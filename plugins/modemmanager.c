/*
 *
 *  Multimedia Messaging Service Daemon - The Next Generation
 *
 *  Copyright (C) 2018 Purism SPC
 *                2020-2021 Chris Talbot <chris@talbothome.com>
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <errno.h>
#include <gio/gio.h>
#include <stdlib.h>
#include <libmm-glib.h>
#include "mms.h"
#include "dbus.h"

// SETTINGS_STORE is synced with services.c
#define SETTINGS_STORE "vvm"

// SETTINGS_GROUP is where we store our settings for this plugin
#define SETTINGS_GROUP "Modem Manager"

//Identifier of the plugin
#define IDENTIFIER     "modemmanager"
//dbus default timeout for Modem
#define MMSD_MM_MODEM_TIMEOUT       20000

#define MMS_MODEMMANAGER_INTERFACE	MMS_SERVICE ".ModemManager"


// Refer to "OMTP_VVM_Specification v1.3, Section 2.8
enum {
	VVM_PROVISION_STATUS_NOT_SET,		//vvmd only, not a VVM Specification
	VVM_PROVISION_STATUS_NEW,
	VVM_PROVISION_STATUS_READY,
	VVM_PROVISION_STATUS_PROVISIONED,
	VVM_PROVISION_STATUS_UNKNOWN,
	VVM_PROVISION_STATUS_BLOCKED,
} e_vvmd_provision_status;

enum {
	MMSD_MM_STATE_NO_MANAGER,
	MMSD_MM_STATE_MANAGER_FOUND,
	MMSD_MM_STATE_NO_MODEM,
	MMSD_MM_STATE_MODEM_FOUND,
	MMSD_MM_STATE_NO_MESSAGING_MODEM,
	MMSD_MM_STATE_MODEM_DISABLED,
	MMSD_MM_STATE_READY
} e_mmsd_connection;

enum {
	MMSD_MM_MODEM_MMSC_MISCONFIGURED,	 //the MMSC is the default value
	MMSD_MM_MODEM_NO_BEARERS_ACTIVE,	//The Modem has no bearers
	MMSD_MM_MODEM_INTERFACE_DISCONNECTED,	//mmsd found the right bearer, but it is disconnected
	MMSD_MM_MODEM_INCORRECT_APN_CONNECTED,	//no APN is connected that matches the settings
	MMSD_MM_MODEM_FUTURE_CASE_DISCONNECTED, //Reserved for future case
	MMSD_MM_MODEM_CONTEXT_ACTIVE		//No error, context activated properly
} mm_context_connection;

struct modem_data {
	struct mms_service *service; //Do not mess with the guts of this in plugin.c!
	GKeyFile *modemsettings;
	//These are pulled from the settings file, and can be set via the Dbus
	int			 provision_status;
	int			 enable_vvm;
	char 			*mailbox_hostname;
	char			*mailbox_port;
	char			*vvm_destination_number;
	char			*mailbox_username;
	char			*mailbox_password;
	char			*vvm_type;
	char			*default_number;
	char			*carrier_prefix;
	// These are for settings the context (i.e. APN settings and if the bearer is active)
	char			*context_interface;		// Bearer interface here (e.g. "wwan0")
	char			*context_path;			// Dbus path of the bearer
	gboolean		 context_active;		// Whether the context is active
	//The Bus org.ofono.mms.ModemManager
	guint			 owner_id;		 
	guint			 registration_id; 
	gulong			 modem_state_watch_id;
	gulong			 sms_wap_signal_id;
	//These are modem manager related settings
	MMManager		*mm;
	guint			 mm_watch_id;
	MMObject		*object; 
	MMModem			*modem;
	char			*path;
	MMSim			*sim;
	gchar			*imsi;
	MMModemMessaging	*modem_messaging;
	MMModemState		 state;
	GPtrArray	 	*device_arr;
	gboolean		 modem_available;
	gboolean		 modem_ready;
	gboolean		 manager_available;
	gboolean		 plugin_registered;
	gboolean		 get_all_sms_timeout;
};

typedef struct {
	MMObject	*object;
	MMModem		*modem;
	MMSim		*sim;
} MmsdDevice;

/* Introspection data for the service we are exporting */
static const gchar introspection_xml_modemmanager[] =
  "<node>"
  "  <interface name='org.ofono.mms.ModemManager'>"
  "    <annotation name='org.ofono.mms.ModemManager' value='OnInterface'/>"
  "    <annotation name='org.ofono.mms.ModemManager' value='AlsoOnInterface'/>"
  "    <method name='PushNotify'>"
  "      <annotation name='org.ofono.mms.ModemManager' value='OnMethod'/>"
  "      <arg type='ay' name='smswap' direction='in'/>"
  "    </method>"
  "    <method name='ViewSettings'>"
  "      <arg type='a{sv}' name='properties' direction='out'/>"
  "    </method>"
  "    <method name='ChangeSettings'>"
  "      <arg type='s' name='setting' direction='in'/>"
  "      <arg type='v' name='value' direction='in'/>"
  "    </method>"
  "    <method name='ProcessMessageQueue'>"
  "      <annotation name='org.ofono.mms.ModemManager' value='OnMethod'/>"
  "    </method>"
  "    <signal name='BearerHandlerError'>"
  "      <annotation name='org.ofono.mms.ModemManager' value='Onsignal'/>"
  "      <arg type='h' name='ContextError'/>"
  "    </signal>"
  "  </interface>"
  "</node>";


static GDBusNodeInfo *introspection_data = NULL;
struct modem_data *modem;

static void mmsd_mm_state(int state);
static void mmsd_modem_available(void);
static void mmsd_modem_unavailable(void);
static void free_device(MmsdDevice *device);
static void cb_mm_manager_new(GDBusConnection *connection, GAsyncResult *res, gpointer user_data);
static void mm_appeared_cb(GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
static void mm_vanished_cb(GDBusConnection *connection, const gchar *name, gpointer user_data);
static int modemmanager_init(void);
static void modemmanager_exit(void);
static void process_mms_process_message_queue(void);
static void mmsd_connect_to_sms_wap(void);
static void mmsd_disconnect_from_sms_wap(void);
static void mmsd_get_all_sms(void);
static void vvmd_unsubscribe_service(void);
static void vvmd_check_subscription_status(void);
static void vvmd_subscribe_service(void);

static void
handle_method_call(GDBusConnection		*connection,
		   const gchar			*sender,
		   const gchar			*object_path,
		   const gchar			*interface_name,
		   const gchar			*method_name,
		   GVariant			*parameters,
		   GDBusMethodInvocation	*invocation,
		   gpointer			 user_data)
{
/*
	if(g_strcmp0(method_name, "PushNotify") == 0)
	{
		GVariant *smswap;
		const unsigned char *data;
		gsize data_len;
		if(modem->modem_ready == TRUE) {

			g_variant_get(parameters, "(@ay)", &smswap);
			data_len = g_variant_get_size(smswap);
			data = g_variant_get_fixed_array(smswap, &data_len, 1);
			DBG("%s",__func__);

			mms_service_push_notify(modem->service, data, data_len);
			g_dbus_method_invocation_return_value(invocation, NULL);

		 } else {
			g_dbus_method_invocation_return_dbus_error(invocation,
								   MMS_MODEMMANAGER_INTERFACE,
								   "Modem is not active!");
		}
	} 
	else if(g_strcmp0(method_name, "ViewSettings") == 0) {
  		GVariantBuilder		 settings_builder;
		GVariant		*settings, *all_settings;

		g_variant_builder_init(&settings_builder, G_VARIANT_TYPE ("a{sv}"));

		g_variant_builder_add_parsed (&settings_builder, 
					      "{'CarrierMMSC', <%s>}", 
				      	      modem->message_center);
				      	      
		g_variant_builder_add_parsed (&settings_builder, 
					      "{'MMS_APN', <%s>}", 
				      	      modem->mms_apn);
		if (modem->MMS_proxy) {
			g_variant_builder_add_parsed (&settings_builder, 
						      "{'CarrierMMSProxy', <%s>}", 
					      	      modem->MMS_proxy);
		} else {
			g_variant_builder_add_parsed (&settings_builder, 
						      "{'CarrierMMSProxy', <%s>}", 
					      	      "NULL");
		}

		if (modem->default_number) {
			g_variant_builder_add_parsed (&settings_builder, 
						      "{'DefaultModemNumber', <%s>}", 
					      	      modem->default_number);
		} else {
			g_variant_builder_add_parsed (&settings_builder, 
						      "{'DefaultModemNumber', <%s>}", 
					      	      "NULL");
		}
				      	      
		g_variant_builder_add_parsed (&settings_builder, 
					      "{'AutoProcessOnConnection', <%b>}", 
				      	      modem->auto_process_on_connection);

		g_variant_builder_add_parsed (&settings_builder, 
					      "{'AutoProcessSMSWAP', <%b>}", 
				      	      modem->autoprocess_sms_wap);

		settings = g_variant_builder_end (&settings_builder);
		
		all_settings = g_variant_new("(*)", settings);
		
		DBG("All Settings: %s", g_variant_print(all_settings, TRUE));
	
		g_dbus_method_invocation_return_value (invocation, all_settings);
	} else if(g_strcmp0(method_name, "ChangeSettings") == 0) {

		GVariant *variantstatus;
  		gchar *dict, *CarrierMMSC, *MMSAPN, *CarrierMMSProxy, *DefaultNumber;
  		gboolean AutoProcessOnConnection, AutoProcessSMSWAP;
  		gboolean close_settings = FALSE;
 
  		
		g_variant_get (parameters, "(sv)", &dict, &variantstatus);
		DBG("Dict: %s", dict);
		if (modem->modemsettings == NULL) {
			close_settings = TRUE;
			modem->modemsettings = mms_settings_open(IDENTIFIER, SETTINGS_STORE);
		}
		
		if(g_strcmp0(dict, "CarrierMMSC") == 0) {
			g_variant_get (variantstatus, "s", &CarrierMMSC);
			g_free(modem->message_center);
			modem->message_center = g_strdup(CarrierMMSC);
			DBG("Carrier MMSC set to %s", modem->message_center);
			g_key_file_set_string(modem->modemsettings, 
					      SETTINGS_GROUP,
				      	      "CarrierMMSC", 
				      	      modem->message_center);

		} else if(g_strcmp0(dict, "MMS_APN") == 0) {
			g_variant_get (variantstatus, "s", &MMSAPN);
			g_free(modem->mms_apn);
			modem->mms_apn = g_strdup(MMSAPN);
			DBG("MMS APN set to %s", modem->mms_apn);
			g_key_file_set_string(modem->modemsettings, 
					      SETTINGS_GROUP,
					      "MMS_APN", 
					      modem->mms_apn);

		} else if(g_strcmp0(dict, "CarrierMMSProxy") == 0) {
			g_variant_get (variantstatus, "s", &CarrierMMSProxy);
			g_free(modem->MMS_proxy);
			modem->MMS_proxy = g_strdup(CarrierMMSProxy);

			g_key_file_set_string(modem->modemsettings, 
					      SETTINGS_GROUP,
				      	      "CarrierMMSProxy", 
				      	      modem->MMS_proxy);

			if(g_strcmp0(modem->MMS_proxy, "NULL") == 0) {
				g_free(modem->MMS_proxy);
				modem->MMS_proxy = NULL;
			}
			DBG("MMS Proxy set to %s", modem->MMS_proxy);

		} else if(g_strcmp0(dict, "DefaultModemNumber") == 0) {
			g_variant_get (variantstatus, "s", &DefaultNumber);
			g_free(modem->default_number);
			modem->default_number = g_strdup(DefaultNumber);

			g_key_file_set_string(modem->modemsettings, 
					      SETTINGS_GROUP,
				      	      "DefaultModemNumber", 
				      	      modem->default_number);

			if(g_strcmp0(modem->default_number, "NULL") == 0) {
				g_free(modem->default_number);
				modem->default_number = NULL;
			}
			DBG("Default Modem Number set to %s", modem->default_number);

		} else if(g_strcmp0(dict, "AutoProcessOnConnection") == 0) {
			g_variant_get (variantstatus, "b", &AutoProcessOnConnection);
			if (modem->auto_process_on_connection == FALSE && AutoProcessOnConnection == TRUE) {
				process_mms_process_message_queue();
			}
			modem->auto_process_on_connection = AutoProcessOnConnection;
			DBG("AutoProcessOnConnection set to %d", modem->auto_process_on_connection);
			g_key_file_set_boolean(modem->modemsettings, 
					       SETTINGS_GROUP,
					       "AutoProcessOnConnection", 
					       modem->auto_process_on_connection);

		} else if(g_strcmp0(dict, "AutoProcessSMSWAP") == 0) {
			g_variant_get (variantstatus, "b", &AutoProcessSMSWAP);
			if (AutoProcessSMSWAP == FALSE && 
					modem->autoprocess_sms_wap == TRUE && 
					modem->modem_available == TRUE) {
				mmsd_disconnect_from_sms_wap();
			}
			if (AutoProcessSMSWAP == TRUE && 
					modem->autoprocess_sms_wap == FALSE) {
				mmsd_connect_to_sms_wap();
				if (modem->modem_ready == TRUE) {
					mmsd_get_all_sms();
				}
			}
			modem->autoprocess_sms_wap = AutoProcessSMSWAP;
			DBG("AutoProcessSMSWAP set to %d", modem->autoprocess_sms_wap);
			g_key_file_set_boolean(modem->modemsettings,
					       SETTINGS_GROUP,
					       "AutoProcessSMSWAP",
					       modem->autoprocess_sms_wap);

		} else {
			if (close_settings == TRUE) {      	      
				mms_settings_close(IDENTIFIER, SETTINGS_STORE,
						   modem->modemsettings, FALSE);
				modem->modemsettings = NULL;
			}

			g_dbus_method_invocation_return_error (invocation,
							       G_DBUS_ERROR,
							       G_DBUS_ERROR_INVALID_ARGS,
							       "Cannot find the Property requested!");
			return;
		}
		if (close_settings == TRUE) {      	      
			mms_settings_close(IDENTIFIER, SETTINGS_STORE,
					   modem->modemsettings, TRUE);
			modem->modemsettings = NULL;
		}
		
		g_dbus_method_invocation_return_value (invocation, NULL);
	}
	else if(g_strcmp0(method_name, "ProcessMessageQueue") == 0) {
		if(modem->modem_ready == TRUE) {
			process_mms_process_message_queue();
			g_dbus_method_invocation_return_value(invocation, NULL);
		} else {
			g_dbus_method_invocation_return_dbus_error(invocation,
								   MMS_MODEMMANAGER_INTERFACE,
								   "Modem is not connected!");
		}
	}
*/
} 

static const GDBusInterfaceVTable interface_vtable =
{
	handle_method_call
};

static void
cb_sms_delete_finish(MMModemMessaging	*modemmessaging,
		     GAsyncResult	*result,
		     MMSms		*sms)
{
	g_autoptr(GError) error = NULL;

	if(mm_modem_messaging_delete_finish(modemmessaging, result, &error)) {
		g_debug("Message delete finish");
	} else {
		g_debug("Couldn't delete SMS - error: %s", error ? error->message : "unknown");
	}
}

static void
process_mms_process_message_queue(void)
{
	if(modem->modem_ready == TRUE) {
		DBG("Processing any unsent/unreceived MMS messages.");	
		/*
		 * Prevent a race condition from the connection turning active to usable
		 * for mmsd-tng
		 */
		sleep(1);

//		activate_bearer(modem->service);
	} else {
		mms_error("Modem is not ready to process any unsent/unreceived MMS messages.");
	}
}

static void
vvm_parse_sync_message(const char *message)
{
	gchar **status_parts, **settings, **single_setting;
	unsigned int status_parts_length, settings_length, adjust_factor;
	DBG("Parsing VVM status message.");

	/*
	 * Example SYNC Message from OTMP VVM Specification:
	 * //VVM:SYNC:ev=NM;id=3446456;c=1;t=v;s=01234567898;dt=02/08/200812:53 +0200;l=30
	 */

	status_parts = g_strsplit(message, ":", 0);
	status_parts_length = g_strv_length (status_parts);

	if (strlen(status_parts[status_parts_length-1]) == 0) {
		adjust_factor = 2;
	} else {
		adjust_factor = 1;
	}
	DBG("Settings: %s", status_parts[status_parts_length-adjust_factor]);
	settings = g_strsplit(status_parts[status_parts_length-adjust_factor], ";", 0);
	settings_length = g_strv_length (settings);

	g_strfreev(status_parts);

	for (int i = 0; i < settings_length; i++) {
		DBG("Setting: %s", settings[i]);
	}
	g_strfreev(settings);

}

static void
vvm_parse_status_message(const char *message)
{
	gchar **status_parts, **settings, **single_setting;
	unsigned int status_parts_length, settings_length, adjust_factor;
	DBG("Parsing VVM status message.");

	/*
	 * Example STATUS Message from OTMP VVM Specification:
	 * //VVM :STATUS:st=N;rc=0;srv=1:10.115.67.251;tui=123;dn=999;ipt=143;spt=25; u=78236487@wirelesscarrier.com;pw=32u4yguetrr34;lang=eng|fre;g_len=25;vs_len=15;pw_len=4-6;smtp_u=super_user@wirelesscarrier.com;smtp_pw=48769463wer;pm=Y;gm=N;vtc=D;vt=1
	 */

	status_parts = g_strsplit(message, ":", 0);
	status_parts_length = g_strv_length (status_parts);

	if (strlen(status_parts[status_parts_length-1]) == 0) {
		adjust_factor = 2;
	} else {
		adjust_factor = 1;
	}
	DBG("Settings: %s", status_parts[status_parts_length-adjust_factor]);
	settings = g_strsplit(status_parts[status_parts_length-adjust_factor], ";", 0);
	settings_length = g_strv_length (settings);

	g_strfreev(status_parts);

	for (int i = 0; i < settings_length; i++) {
		DBG("Setting: %s", settings[i]);
		single_setting = g_strsplit(settings[i], "=", 0);

		if (g_strcmp0(single_setting[0], "st") == 0) { //Provisioning Status
			if (g_strcmp0(single_setting[1], "N") == 0) {
				// N = Subscriber New
				modem->provision_status = VVM_PROVISION_STATUS_NEW;
				DBG("Provisioning Status: New.");
			} else if (g_strcmp0(single_setting[1], "R") == 0) {
				// R = Subscriber Ready
				modem->provision_status = VVM_PROVISION_STATUS_READY;
				DBG("Provisioning Status: Ready.");
			} else if (g_strcmp0(single_setting[1], "P") == 0) {
				// P = Subscriber Provisioned
				modem->provision_status = VVM_PROVISION_STATUS_PROVISIONED;
				DBG("Provisioning Status: Provisioned.");
			} else if (g_strcmp0(single_setting[1], "U") == 0) {
				// U = Subscriber Unknown
				modem->provision_status = VVM_PROVISION_STATUS_UNKNOWN;
				DBG("Provisioning Status: Unknown.");
			} else if (g_strcmp0(single_setting[1], "B") == 0) {
				// B = Subscriber Blocked
				modem->provision_status = VVM_PROVISION_STATUS_BLOCKED;
				DBG("Provisioning Status: Blocked.");
			} else {
				mms_error("Unknown Provisioning status!");
			}
			g_key_file_set_integer(modem->modemsettings, SETTINGS_GROUP,
					      "ProvisionStatus", modem->provision_status);

		} else if (g_strcmp0(single_setting[0], "rc") == 0) { //Return Code
			if (g_strcmp0(single_setting[1], "0") == 0) {
				//0 = Success
				DBG("Return Code: Success.");
			} else if (g_strcmp0(single_setting[1], "1") == 0) {
				//1 = System error
				DBG("Return Code: System error.");
			} else if (g_strcmp0(single_setting[1], "2") == 0) {
				//2 = Subscriber error
				DBG("Return Code: Subscriber error.");
			} else if (g_strcmp0(single_setting[1], "3") == 0) {
				//3 = Mailbox unknown
				DBG("Return Code: Mailbox unknown.");
			} else if (g_strcmp0(single_setting[1], "4") == 0) {
				//4 = VVM not activated
				DBG("Return Code: VVM not activated.");
			} else if (g_strcmp0(single_setting[1], "5") == 0) {
				//5 = VVM not provisioned
				DBG("Return Code: System error.");
			} else if (g_strcmp0(single_setting[1], "6") == 0) {
				//6 = VVM client unknown
				DBG("Return Code: VVM not provisioned.");
			} else if (g_strcmp0(single_setting[1], "7") == 0) {
				//7 = VVM mailbox not initialised
				DBG("Return Code: VVM mailbox not initialised.");
			} else {
				mms_error("Unknown Return Code.");
			}

		} else if (g_strcmp0(single_setting[0], "srv") == 0) { //Mailbox Hostname
			g_free(modem->mailbox_hostname);
			modem->mailbox_hostname = g_strdup(single_setting[1]);
			DBG("Setting Mailbox Hostname to %s", modem->mailbox_hostname);
			g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
					      "MailboxHostname", modem->mailbox_hostname);

		} else if (g_strcmp0(single_setting[0], "ipt") == 0) { //Mailbox Port
			g_free(modem->mailbox_port);
			modem->mailbox_port = g_strdup(single_setting[1]);
			DBG("Setting Mailbox Port to %s", modem->mailbox_port);

			g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
					      "MailboxPort", modem->mailbox_port);

		} else if (g_strcmp0(single_setting[0], "u") == 0) { //Mailbox Username
			g_free(modem->mailbox_username);
			modem->mailbox_username = g_strdup(single_setting[1]);
			DBG("Setting Mailbox Username to %s", modem->mailbox_username);

			g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
					      "MailboxUsername", modem->mailbox_username);

		} else if (g_strcmp0(single_setting[0], "pw") == 0) { //Mailbox Password
			g_free(modem->mailbox_password);
			modem->mailbox_password = g_strdup(single_setting[1]);
			DBG("Setting Mailbox Password to %s", modem->mailbox_password);

			g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
					      "MailboxPassword", modem->mailbox_password);


		} else {
			DBG("Not procesing %s", single_setting[0]);
		}
		g_strfreev(single_setting);
	}
	g_strfreev(settings);

	if (modem->provision_status == VVM_PROVISION_STATUS_PROVISIONED) {
		if (modem->enable_vvm) {
			vvmd_subscribe_service();
			DBG("Unsubscribing to your carrier's VVM service");
		}
	} else if (modem->provision_status == VVM_PROVISION_STATUS_NEW || 
	 	   modem->provision_status == VVM_PROVISION_STATUS_READY) {
		if (!modem->enable_vvm) {
			DBG("Unsubscribing from your carrier's VVM service");
			vvmd_unsubscribe_service();
		}
	} else if (modem->provision_status == VVM_PROVISION_STATUS_UNKNOWN) {
		mms_error("Provisioning Status is unknown! Contact your Carrier");
	} else if (modem->provision_status == VVM_PROVISION_STATUS_BLOCKED) {
		mms_error("Provisioning Status is blocked! Contact your Carrier");
	}
}


static void
mmsd_process_sms(MMSms	*sms)
{
	gchar		*message;
	const char	*path;

	message = mm_sms_dup_text(sms);
	if(message) {
		char *status_prefix = g_strdup_printf("%s:STATUS:", modem->carrier_prefix);
		char *sync_prefix = g_strdup_printf("%s:SYNC:", modem->carrier_prefix);
		DBG("Message: %s", message);

		if(g_str_has_prefix(message, status_prefix)) {
			vvm_parse_status_message(message);
		} else if(g_str_has_prefix(message, sync_prefix)) {
			DBG("This is a VVM sync message.");
			if(modem->modem_ready == TRUE) {
				vvm_parse_sync_message(message);
			} else {
				DBG("Modem is not connected.");	
			}
		} else {
			DBG("This is not a VVM related Message");
		}

		if(g_str_has_prefix(message, status_prefix) || g_str_has_prefix(message, sync_prefix)) {
			path = mm_sms_get_path(sms);
			if(path) {
				mms_error("Deleting SMS!");				
				mm_modem_messaging_delete(modem->modem_messaging,
							  path,
							  NULL,
							  (GAsyncReadyCallback)cb_sms_delete_finish,
							  sms);
			} else {
				mms_error("vvmd cannot delete SMS at this time!");
			}
		}
		g_free(status_prefix);
		g_free(sync_prefix);
		g_free(message);

	}
}

static void
cb_sms_state_change(MMSms	*sms,
		    GParamSpec	*pspec,
		    gpointer 	*user_data)
{
	MMSmsState state;

	state = mm_sms_get_state(sms);
	DBG("%s: state %s", __func__,
		  mm_sms_state_get_string(mm_sms_get_state(sms)));

	if(state == MM_SMS_STATE_RECEIVED) {
		mmsd_process_sms(sms);
	}
}

static void
mmsd_check_pdu_type(MMSms	*sms)
{
	MMSmsState	state;
	MMSmsPduType	pdu_type;

	pdu_type = mm_sms_get_pdu_type(sms);
	state = mm_sms_get_state(sms);
	switch(pdu_type) {
		case MM_SMS_PDU_TYPE_CDMA_DELIVER:
		case MM_SMS_PDU_TYPE_DELIVER:

			if(state == MM_SMS_STATE_RECEIVED) {
				mmsd_process_sms(sms);
			}

			if(state == MM_SMS_STATE_RECEIVING) {
				// The first chunk of a multipart SMS has been
				// received -> wait for MM_SMS_STATE_RECEIVED
				g_signal_connect(sms,
						 "notify::state",
						 G_CALLBACK(cb_sms_state_change),
						 NULL);
			}
		break;

		case MM_SMS_PDU_TYPE_STATUS_REPORT:
		case MM_SMS_PDU_TYPE_SUBMIT:
			DBG("This is not an SMS being received, do not care");
			break;

		case MM_SMS_PDU_TYPE_UNKNOWN:
			DBG("Unknown PDU type");
			break;

		default:
			DBG("PDU type not handled");
	}
}


static void
cb_sms_list_new_ready(MMModemMessaging	*modemmessaging,
		      GAsyncResult	*result,
		      gchar		*path)
{
	GList			*l, *list;
	g_autoptr(GError)	 error = NULL;
	MMSms			*sms;

	list = mm_modem_messaging_list_finish(modemmessaging, result, &error);

	if(!list) {
		mms_error("Couldn't get SMS list - error: %s", 
			  error ? error->message : "unknown");
	} else {
	for(l = list; l; l = g_list_next(l)) {
		//We are searching for the SMS from the list that is new
		if(!g_strcmp0(mm_sms_get_path(MM_SMS(l->data)), path)) {
			sms = g_object_ref(MM_SMS(l->data));
			mmsd_check_pdu_type(sms);
			break;
		}
	}
	g_list_free_full(list, g_object_unref);
	g_free(path);
	}
}

static gboolean
cb_dbus_signal_sms_added(MMModemMessaging	*device,
			 gchar			*const_path,
			 gpointer		 user_data)
{
	gchar *path;
	path = g_strdup(const_path);
	mm_modem_messaging_list(modem->modem_messaging,
				NULL,
				(GAsyncReadyCallback)cb_sms_list_new_ready,
				path);
	return TRUE;
}

static void
cb_sms_list_all_ready(MMModemMessaging	*modemmessaging,
		      GAsyncResult	*result,
		      gpointer		 user_data)
{
	GList			*l, *list;
	g_autoptr(GError)	error = NULL;
	MMSms			*sms;

	list = mm_modem_messaging_list_finish(modemmessaging, result, &error);

	if(!list) {
		g_debug("Couldn't get SMS list - error: %s", error ? error->message : "unknown");
	} else {
		for(l = list; l; l = g_list_next(l)) {
			sms = g_object_ref(MM_SMS(l->data));
			mmsd_check_pdu_type(sms);
		}
	}
}

static gboolean 
mmsd_get_all_sms_timeout(gpointer user_data)
{
	DBG("Removing timeout to mmsd_get_all_sms()");
	modem->get_all_sms_timeout = FALSE;

	return FALSE;
}

static void
mmsd_get_all_sms(void)
{
	g_return_if_fail(MM_IS_MODEM_MESSAGING(modem->modem_messaging));
	
	if (modem->get_all_sms_timeout == FALSE) {
		DBG("Searching for any new SMS WAPs...");

		// This is needed in case mmsd-tng is 
		// trying to come out of suspend
		sleep(1);

		mm_modem_messaging_list(modem->modem_messaging,
					NULL,
					(GAsyncReadyCallback)cb_sms_list_all_ready,
					NULL);
		modem->get_all_sms_timeout = TRUE;
		DBG("Adding timeout to mmsd_get_all_sms()");
		//Adding five second timeout
		g_timeout_add_seconds(5,mmsd_get_all_sms_timeout, NULL);
	} else {
		DBG("mmsd_get_all_sms() timed out!");
	}
}

static void
mmsd_disconnect_from_sms_wap(void)
{
	MmGdbusModemMessaging *gdbus_sms;
	if (modem->modem_messaging == NULL) {
		mms_error("SMS WAP Disconnect: There is no modem with messaging capabilities!");
		return;
	}
	DBG("Stopping watching SMS WAPs");
	gdbus_sms = MM_GDBUS_MODEM_MESSAGING(modem->modem_messaging);
	g_signal_handler_disconnect (gdbus_sms,
				     modem->sms_wap_signal_id);
}

static void
mmsd_connect_to_sms_wap(void)
{
	MmGdbusModemMessaging *gdbus_sms;
	if (modem->modem_messaging == NULL) {
		mms_error("SMS WAP Connect: There is no modem with messaging capabilities!");
		return;
	}
	gdbus_sms = MM_GDBUS_MODEM_MESSAGING(modem->modem_messaging);
	DBG("Watching for new SMS WAPs");
	modem->sms_wap_signal_id = g_signal_connect(gdbus_sms,
			 "added",
			  G_CALLBACK(cb_dbus_signal_sms_added),
			  NULL);
}

static gboolean
mmsd_mm_init_modem(MMObject *obj)
{

	modem->modem_messaging = mm_object_get_modem_messaging(MM_OBJECT(obj)); 
	if (modem->modem_messaging == NULL) {
		return FALSE;
	}

	modem->object = obj;
	modem->modem = mm_object_get_modem(MM_OBJECT(obj));
	modem->path = mm_modem_dup_path(modem->modem);

	g_dbus_proxy_set_default_timeout(G_DBUS_PROXY(modem->modem),
					 MMSD_MM_MODEM_TIMEOUT);


	
	DBG("%s", __func__);

	return TRUE;
}

static void
free_device(MmsdDevice *device)
{
	if(!device)
		return;

	g_clear_object(&device->sim);
	g_clear_object(&device->modem);
	g_clear_object(&device->object);
	g_free(device);
}

static gboolean
device_match_by_object(MmsdDevice	*device,
		       GDBusObject	*object)

{
	g_return_val_if_fail(G_IS_DBUS_OBJECT(object), FALSE);
	g_return_val_if_fail(MM_OBJECT(device->object), FALSE);

	return object == G_DBUS_OBJECT(device->object);
}

static void
mmsd_mm_add_object(MMObject *obj)
{
	MmsdDevice 	   *device;
	const gchar	   *object_path;
//	const gchar *const *modem_number_ref;
//	MMSim		   *sim;
//	const gchar 	   *country_code;
	g_autoptr(GError)   error = NULL;
//	gchar 		   *modem_number_formatted;

	object_path = g_dbus_object_get_object_path(G_DBUS_OBJECT(obj));

	g_return_if_fail(object_path);
	//Begin if statement
	if(g_ptr_array_find_with_equal_func(modem->device_arr,
					    obj,
					    (GEqualFunc)device_match_by_object,
					    NULL)) {
	//End if statement
	DBG("Device %s already added", object_path);
	return;
	}

	if (modem->modem_available) {
		DBG("There is already a modem registered!");
		return;
	}
/*
	if (modem->default_number != NULL) {
		DBG("Checking if this modem number matches: %s", modem->default_number);
		sim = mm_modem_get_sim_sync(mm_object_get_modem(MM_OBJECT(obj)),
						   NULL,
				 		   &error);
		if(error != NULL) {
		        mms_error ("Error Getting Sim: %s", error->message);
			return;
		}
		country_code = get_country_iso_for_mcc (mm_sim_get_imsi(sim));
		modem_number_ref = mm_modem_get_own_numbers (mm_object_get_modem(MM_OBJECT(obj)));
		if (modem_number_ref == NULL) {
			mms_error("Could not get number!");
			return;
		}
		modem_number_formatted = mms_message_format_number_e164(*modem_number_ref, country_code, FALSE);
		DBG("Formatted Number %s", modem_number_formatted);
		if(g_strcmp0(modem_number_formatted, modem->default_number) != 0) {
			g_free(modem_number_formatted);
			mms_error("This modem does not match default number!");
			return;
		}
		g_free(modem_number_formatted);
	} else {
		DBG("Not checking for a default Modem");
	}
*/
	DBG("Added device at: %s", object_path);

	if (mmsd_mm_init_modem(obj) == TRUE) {
		device = g_new0(MmsdDevice, 1);
		device->object = g_object_ref(MM_OBJECT(obj));
		device->modem = mm_object_get_modem(MM_OBJECT(obj));
		g_ptr_array_add(modem->device_arr, device);

		mmsd_mm_state(MMSD_MM_STATE_MODEM_FOUND);
	} else {
		mmsd_mm_state(MMSD_MM_STATE_NO_MESSAGING_MODEM);
	}
}

static void
mmsd_mm_get_modems(void)
{
	GList *list, *l;
	gboolean has_modem = FALSE;

	g_return_if_fail(MM_IS_MANAGER(modem->mm));

	list = g_dbus_object_manager_get_objects(G_DBUS_OBJECT_MANAGER(modem->mm));

	for(l = list; l != NULL; l = l->next) {
		if(!mm_object_peek_modem_messaging(l->data))
			continue;

		has_modem = TRUE;
		mmsd_mm_add_object(MM_OBJECT(l->data));
	}

	if(!has_modem) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MODEM);
	} else if(list) {
		g_list_free_full(list, g_object_unref);
	}
}


static void
cb_object_added(GDBusObjectManager 	*manager,
		GDBusObject		*object,
		gpointer		 user_data)
{
	DBG("%s", __func__);
	if(mm_object_peek_modem_messaging(MM_OBJECT(object))) {
		mms_error("New Object does not have Messaging feature, ignoring....");
		mmsd_mm_add_object(MM_OBJECT(object));
	}


}

static void
cb_object_removed(GDBusObjectManager	*manager,
		  GDBusObject		*object,
		  gpointer		 user_data)
{
	guint index;

	g_return_if_fail(G_IS_DBUS_OBJECT(object));
	g_return_if_fail(G_IS_DBUS_OBJECT_MANAGER(manager));
	//Begin if statement
	if(g_ptr_array_find_with_equal_func(modem->device_arr,
					    object,
					    (GEqualFunc)device_match_by_object,
					    &index)) {
	//End if Statement
		g_ptr_array_remove_index_fast(modem->device_arr, index);
	}

	if(MM_OBJECT(object) == modem->object) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MODEM);
	}

	DBG("Modem removed: %s", g_dbus_object_get_object_path(object));
}


static void
cb_name_owner_changed(GDBusObjectManager	*manager,
		      GDBusObject		*object,
		      gpointer			 user_data)
{
	gchar *name_owner;

	name_owner = g_dbus_object_manager_client_get_name_owner(G_DBUS_OBJECT_MANAGER_CLIENT(manager));

	if(!name_owner) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MANAGER);
	}

	DBG("Name owner changed");

	g_free(name_owner);
}

static void
cb_mm_manager_new(GDBusConnection 	*connection,
		  GAsyncResult		*res,
		  gpointer		 user_data)
{
	gchar		 	*name_owner;
	g_autoptr(GError)	 error = NULL;


	modem->mm = mm_manager_new_finish(res, &error);
	modem->device_arr = g_ptr_array_new_with_free_func((GDestroyNotify) free_device);

	if(modem->mm) {

		mmsd_mm_state(MMSD_MM_STATE_MANAGER_FOUND);

		g_signal_connect(modem->mm,
				 "interface-added",
				 G_CALLBACK(cb_object_added),
				 NULL);

		g_signal_connect(modem->mm,
				 "object-added",
				 G_CALLBACK(cb_object_added),
				 NULL);

		g_signal_connect(modem->mm,
				 "object-removed",
				 G_CALLBACK(cb_object_removed),
				 NULL);

		g_signal_connect(modem->mm,
				 "notify::name-owner",
				 G_CALLBACK(cb_name_owner_changed),
				 NULL);

		name_owner = g_dbus_object_manager_client_get_name_owner(G_DBUS_OBJECT_MANAGER_CLIENT(modem->mm));
		DBG("ModemManager found: %s\n", name_owner);
		g_free(name_owner);

		mmsd_mm_get_modems();

	} else {
		mms_error("Error connecting to ModemManager: %s\n", error->message);

		mmsd_mm_state(MMSD_MM_STATE_NO_MANAGER);
	}
}

static void
mmsd_mm_get_modem_state(void)
{
	g_autoptr(GError) error = NULL;

	if(!modem->modem) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MODEM);
		return;
	}

	if(modem->state < MM_MODEM_STATE_ENABLED) {
			DBG("Something May be wrong with the modem, checking....");
			switch(modem->state) {
			case MM_MODEM_STATE_FAILED:
				DBG("MM_MODEM_STATE_FAILED");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_UNKNOWN:
				DBG("MM_MODEM_STATE_UNKNOWN");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_LOCKED:
				DBG("MM_MODEM_STATE_FAILED");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_INITIALIZING:
				DBG("MM_MODEM_STATE_INITIALIZING");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_DISABLED:
				DBG("MM_MODEM_STATE_DISABLED");
				DBG("Turning on Modem....");
				mm_modem_set_power_state_sync(modem->modem, MM_MODEM_POWER_STATE_ON, NULL, &error);
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_DISABLING:
				DBG("MM_MODEM_STATE_DISABLING");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			case MM_MODEM_STATE_ENABLING:
				DBG("MM_MODEM_STATE_ENABLING");
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
			default:
				DBG("MM_MODEM_OTHER_BAD_STATE: %d", modem->state);
				mmsd_mm_state(MMSD_MM_STATE_MODEM_DISABLED);
				return;
		}
	}
	DBG("MM_MODEM_GOOD_STATE: %d", modem->state);
	mmsd_mm_state(MMSD_MM_STATE_READY);

	/* Automatically process unsent/unreceived messages when the modem is connected */
	if(modem->state == MM_MODEM_STATE_CONNECTED) {
		process_mms_process_message_queue();
	}
	return;
}

static void 
modem_state_changed_cb(MMModem			*cb_modem,
		       MMModemState		 old,
		       MMModemState	 	 new,
		       MMModemStateChangeReason	 reason)
{
	DBG("State Change: Old State: %d New State: %d, Reason: %d", old, new, reason);
	modem->state = new;
	mmsd_mm_get_modem_state();
}

static void
mmsd_mm_state(int state)
{
	switch(state) {
		case MMSD_MM_STATE_MODEM_FOUND:
			DBG("MMSD_MM_STATE_MODEM_FOUND");
			if(!modem->modem_available) {
				mmsd_modem_available();
			}
			break;
		case MMSD_MM_STATE_NO_MODEM:
			if(modem->modem_available) {
				mmsd_modem_unavailable();
				DBG("Modem vanished, Disabling plugin");
			} else {
				mms_error("Could not connect to modem");
			}
			modem->modem_available = FALSE;
			modem->modem_ready = FALSE;
			DBG("MMSD_MM_STATE_NO_MODEM");
			break;

		case MMSD_MM_STATE_NO_MESSAGING_MODEM:
			DBG("Modem has no messaging capabilities");
			DBG("MMSD_MM_STATE_NO_MESSAGING_MODEM");
			modem->modem_available = FALSE;
			modem->modem_ready = FALSE;
			break;

		case MMSD_MM_STATE_MODEM_DISABLED:
			DBG("MMSD_MM_STATE_MODEM_DISABLED");
//			mms_service_set_bearer_handler(modem->service, NULL, NULL);
			modem->modem_ready = FALSE;
			break;

		case MMSD_MM_STATE_MANAGER_FOUND:
			if(!modem->manager_available) {
				modem->manager_available = TRUE;
			}
			DBG("MMSD_MM_STATE_MANAGER_FOUND");
			break;

		case MMSD_MM_STATE_NO_MANAGER:
			if(modem->manager_available) {
				g_clear_object(&modem->mm);
				modem->modem_available = FALSE;
				modem->modem_ready = FALSE;
			} else {
				mms_error("Could not connect to ModemManager");
			}
			modem->manager_available = FALSE;
			DBG("MMSD_MM_STATE_NO_MANAGER");
			break;

		case MMSD_MM_STATE_READY:
			DBG("MMSD_MM_STATE_READY");
			modem->modem_ready = TRUE;
			DBG("Setting Bearer Handler");
//			mms_service_set_bearer_handler(modem->service, bearer_handler, modem);
			mmsd_get_all_sms();
			break;

		default:
			g_return_if_reached();
	}
}

static void
mm_appeared_cb(GDBusConnection	*connection,
	       const gchar 	*name,
	       const gchar 	*name_owner,
	       gpointer		 user_data)
{
	g_assert(G_IS_DBUS_CONNECTION(connection));
	DBG("Modem Manager appeared");

	mm_manager_new(connection,
		       G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE,
		       NULL,
		       (GAsyncReadyCallback) cb_mm_manager_new,
		       NULL);

}

static void
mm_vanished_cb(GDBusConnection	*connection,
	       const gchar	*name,
	       gpointer		 user_data)
{
	g_assert(G_IS_DBUS_CONNECTION(connection));

	DBG("Modem Manager vanished");
	mmsd_mm_state(MMSD_MM_STATE_NO_MANAGER);
}
/*
static void bearer_handler(gboolean    active, 
			   void 	*user_data)
{

	struct modem_data *modem = user_data;
	gint32 response;
	GDBusConnection	*connection = mms_dbus_get_connection ();
	/* Check for any errors within the context */
/*	response = set_context();
	if(response != MMSD_MM_MODEM_CONTEXT_ACTIVE) {
		DBG("Set MMSC: %s, Set Proxy: %s, Set MMS APN: %s", modem->message_center, modem->MMS_proxy, modem->mms_apn);
		g_dbus_connection_emit_signal(connection,
					      NULL,
					      MMS_PATH,
					      MMS_MODEMMANAGER_INTERFACE,
					      "BearerHandlerError",
					      g_variant_new("(h)", response),
					      NULL);
	}
	DBG("At Bearer Handler: path %s active %d context_active %d", modem->path, active, modem->context_active);
	if(active == TRUE && modem->context_active == TRUE) {
		DBG("active and context_active, bearer_notify");
		mms_service_bearer_notify(modem->service, TRUE, modem->context_interface, modem->MMS_proxy);
		return;
	} else if(active == TRUE && modem->context_active == FALSE) {
		DBG("Error activating context!");
		mms_service_bearer_notify(modem->service, FALSE, NULL, NULL);
		return;
	}

	DBG("checking for failure");
	if(active == FALSE && modem->context_active == FALSE) {
		mms_error("Context not active!");
		mms_service_bearer_notify(modem->service, FALSE, NULL, NULL);
		return;
	} else {
		DBG("No failures");
		mms_service_bearer_notify(modem->service, FALSE, modem->context_interface, modem->MMS_proxy);
		return;
	}
}
 
static int set_context(void)
{
	guint 			 max_bearers, active_bearers;
	GList 			*bearer_list, *l;
	MMBearer 		*modem_bearer;
	MMBearerProperties 	*modem_bearer_properties;
	const gchar 		*apn;
	gboolean 		 interface_disconnected;
	gboolean 		 bearer_connected;

	DBG("Setting Context...");
	if(modem->context_active) {
		g_free(modem->context_interface);
		g_free(modem->context_path);
	}
	modem->context_active = FALSE;
	interface_disconnected = FALSE;
	mms_service_set_mmsc(modem->service, modem->message_center);
	max_bearers = mm_modem_get_max_active_bearers(modem->modem);
	DBG("Max number of bearers: %d", max_bearers);
	bearer_list = mm_modem_list_bearers_sync(modem->modem, NULL, NULL);
	active_bearers = 0;
	if(bearer_list != NULL) {
		for(l = bearer_list; l != NULL; l = l->next) {
			active_bearers = active_bearers + 1;
			modem_bearer =(MMBearer *) l->data;
			modem_bearer_properties = mm_bearer_get_properties(modem_bearer);
			apn = mm_bearer_properties_get_apn(modem_bearer_properties);
			DBG("Current Context APN: %s, mmsd-tng settings MMS APN: %s", apn, modem->mms_apn);
			bearer_connected = mm_bearer_get_connected(modem_bearer);
			if(g_strcmp0(apn, modem->mms_apn) == 0 ) {
				if(modem->state != MM_MODEM_STATE_CONNECTED) {
					DBG("The modem interface is reporting it is disconnected!");
					DBG("Reported State: %d", modem->state);
					interface_disconnected = TRUE;
				} else if(!bearer_connected) {
					mms_error("The proper context is not connected!");
					interface_disconnected = TRUE;
				} else {
					DBG("You are connected to the correct APN! Enabling context...");
					modem->context_interface = mm_bearer_dup_interface(modem_bearer);
					modem->context_path = mm_bearer_dup_path(modem_bearer);
					modem->context_active = TRUE;
				}
			}
		}
		g_list_free(bearer_list);
		g_list_free(l);
		if(!modem->context_active) { // I did not find the right context I wanted.
			if(active_bearers == max_bearers) {
				if(interface_disconnected) {
					return MMSD_MM_MODEM_INTERFACE_DISCONNECTED;
				} else {
					DBG("The modem is not connected to the correct APN!");
					return MMSD_MM_MODEM_INCORRECT_APN_CONNECTED;
				}
			} else if(active_bearers == 0) {
				DBG("The modem bearer is disconnected! Please enable modem data");
				return MMSD_MM_MODEM_NO_BEARERS_ACTIVE;
			} 
			 else if(active_bearers < max_bearers) {
				/*
				 * TODO: Modem manager does not support this yet, but this is
				 *	 where to add code when Modem manager supports multiple
				 *	 contexts and/or a seperate MMS context.
				 *	 The Pinephone and Librem 5 only support 
				 *	 one active bearer as well
				 */ 
/*				mms_error("This is a stub for adding a new context/bearer, but Modem Manager does not support this yet."); 
				return MMSD_MM_MODEM_FUTURE_CASE_DISCONNECTED;
			}
		}
	} else {
		mms_error("There are no modem bearers! Please enable modem data");
		return MMSD_MM_MODEM_NO_BEARERS_ACTIVE;
	}
	
	if(g_strcmp0(modem->message_center, "http://mmsc.invalid") == 0) {
		mms_error("The MMSC is not configured! Please configure the MMSC and restart mmsd-tng.");
		return MMSD_MM_MODEM_MMSC_MISCONFIGURED;
	} 
	return MMSD_MM_MODEM_CONTEXT_ACTIVE;
}
*/

static void
cb_sms_send_finish (MMSms        *sms,
                    GAsyncResult *result,
                    gpointer      user_data)
{
	g_autoptr(GError)  error = NULL;
	gboolean           fin;

	fin = mm_sms_send_finish (sms, result, &error);

	if (!fin) {
		mms_error ("Couldn't send SMS - error: %s", error ? error->message : "unknown");
	} else {
		DBG ("Successfully sent SMS: %s", mm_sms_get_path (sms));
		const char	*path;
		path = mm_sms_get_path(sms);
		if(path) {
			mm_modem_messaging_delete(modem->modem_messaging,
						  path,
						  NULL,
						  (GAsyncReadyCallback)cb_sms_delete_finish,
						  sms);
		} else {
			mms_error("vvmd cannot process SMS at this time!");
		}


	}
}


static void
cb_sms_create_finish (MMModemMessaging *modem,
                      GAsyncResult     *result,
                      gpointer          user_data)
{
	MMSms             *sms;
	g_autoptr(GError)  error = NULL;

	sms = mm_modem_messaging_create_finish (modem, result, &error);

	if (!sms) {
		DBG ("Couldn't create new SMS - error: %s", error ? error->message : "unknown");
	} else {
		DBG ("Successfully created new SMS: %s", mm_sms_get_path (sms));

		mm_sms_send (sms,
           		     NULL,
           		     (GAsyncReadyCallback)cb_sms_send_finish,
                	     NULL);

		g_object_unref (sms);
  }
}


static gboolean
vvmd_mm_create_sms (const gchar *message)
{
	MMSmsProperties *properties;
	gboolean	 delivery_report = 0;

	properties = mm_sms_properties_new ();

	mm_sms_properties_set_text (properties, message);
	mm_sms_properties_set_number (properties, modem->vvm_destination_number);
	mm_sms_properties_set_delivery_report_request (properties, delivery_report);

	DBG("Creating new SMS");

	mm_modem_messaging_create (modem->modem_messaging,
                          	   properties,
                             	   NULL,
                             	   (GAsyncReadyCallback)cb_sms_create_finish,
                             	   NULL);

	g_object_unref (properties);

	return TRUE;
}


static void vvmd_unsubscribe_service(void)
{
	char *message = NULL;
	DBG("Unsubscribing from your carrier's VVM service");
	if (g_strcmp0(modem->vvm_type, "cvvm") == 0) {
		//Refer to Android's Dialer, OmtpCvvmMessageSender.java
		// dt = device type, 6 = no VTT (transcription) support
		message = g_strdup("Deactivate:dt=6");
	} else if (g_strcmp0(modem->vvm_type, "otmp") == 0) {
		//TODO: Fix this for other carriers
		DBG("I do not know how to unsubscribe from an OMTP VVM service!");
	} else {
		mms_error("Unknown type of VVM service.");
	}
	if (message) {
		vvmd_mm_create_sms (message);
	}
	g_free(message);
}

static void vvmd_check_subscription_status(void)
{
	char *message = NULL;
	DBG("Unsubscribing from your carrier's VVM service");
	if (g_strcmp0(modem->vvm_type, "cvvm") == 0) {
		//Refer to Android's Dialer, OmtpCvvmMessageSender.java
		//dt = device type, 6 = no VTT (transcription) support
		message = g_strdup("STATUS:dt=6");
	} else if (g_strcmp0(modem->vvm_type, "otmp") == 0) {
		//TODO: Fix this for other carriers
		DBG("I do not know how check the status of an OMTP VVM service!");
	} else {
		mms_error("Unknown type of VVM service.");
	}
	if (message) {
		vvmd_mm_create_sms (message);
	}
	g_free(message);
}

static void vvmd_subscribe_service(void)
{
	char *message = NULL;
	if(modem->provision_status == VVM_PROVISION_STATUS_NOT_SET) {
		/*
		 * I have experiementally seen that I can unsubscribe to see
		 * The status of VVM, then take action based on that
		 */
		DBG("Your provisioning status is unknown");
		DBG("Checking Subscription Status");
		vvmd_check_subscription_status();
		return;
	} else {
		DBG("You are not be subscribed to your carrier's VVM service");
	}

	if (g_strcmp0(modem->vvm_type, "cvvm") == 0) {
		//Refer to Android's Dialer, OmtpCvvmMessageSender.java
		//dt = device type, 6 = no VTT (transcription) support
		message = g_strdup("Activate:dt=6");
	} else if (g_strcmp0(modem->vvm_type, "otmp") == 0) {
		//TODO: Fix this for other carriers
		DBG("I do not know how to subscribe to an OMTP VVM service!");
	} else {
		mms_error("Unknown type of VVM service.");
	}
	if (message) {
		vvmd_mm_create_sms (message);
	}
	g_free(message);
}

static void mmsd_modem_available(void)
{
	g_autoptr(GError) error = NULL;

	modem->modem_available = TRUE;
	if(modem->modem) {
		const gchar *const *modem_number_ref;
		if(modem->plugin_registered == FALSE) {
			DBG("Registering Modem Manager MMS Service");
//			mms_service_register(modem->service);
//			modem->modemsettings = mms_service_get_keyfile(modem->service);
			modem->plugin_registered = TRUE;
		}

		MmGdbusModem *gdbus_modem;

		modem->sim = mm_modem_get_sim_sync(modem->modem,
						   NULL,
				 		   &error);
				 
		if(error == NULL) {
			modem->imsi = mm_sim_dup_imsi(modem->sim);

//			mms_service_set_country_code(modem->service, 
//						     mm_sim_get_imsi(modem->sim));
		} else {
		        mms_error ("Error Getting Sim: %s", error->message);
		}

		modem_number_ref = mm_modem_get_own_numbers (modem->modem);
		if (modem_number_ref != NULL) {
//			mms_service_set_own_number(modem->service, 
//					           *modem_number_ref);
		} else {
			mms_error("Could not get modem number!");
		}

		mmsd_connect_to_sms_wap();

		if (modem->provision_status == VVM_PROVISION_STATUS_NOT_SET ||
		    modem->provision_status == VVM_PROVISION_STATUS_UNKNOWN || 
		    modem->provision_status == VVM_PROVISION_STATUS_BLOCKED) {
			DBG("Checking status of carrier subscription");
			vvmd_check_subscription_status();
		} else if (modem->provision_status == VVM_PROVISION_STATUS_PROVISIONED) {
			if (modem->enable_vvm) {
				DBG("Subscribing to your carrier's VVM service");
				vvmd_subscribe_service();
			} else {
				DBG("VVM is disabled, not subscribing to your carrier's VVM service");
			}
		} else if (modem->provision_status == VVM_PROVISION_STATUS_NEW || 
		 	   modem->provision_status == VVM_PROVISION_STATUS_READY) {
			if (!modem->enable_vvm) {
				vvmd_unsubscribe_service();
				DBG("Unsubscribing to your carrier's VVM service");
			} else {
				DBG("VVM is enabled and subscribed!");
			}
		}

		gdbus_modem = MM_GDBUS_MODEM(modem->modem);

		modem->modem_state_watch_id = g_signal_connect(gdbus_modem, 
							       "state-changed",
								G_CALLBACK(modem_state_changed_cb), 
							       NULL);
		modem->state = mm_modem_get_state(modem->modem);
		mmsd_mm_get_modem_state();
	} else {
		mms_error("Something very bad happened at mmsd_modem_available()!");
	}

}

static void mmsd_modem_unavailable(void)
{
	MmGdbusModem *gdbus_modem;

	DBG("Disabling Bearer Handler");
	gdbus_modem = MM_GDBUS_MODEM(modem->modem);
//	mms_service_set_bearer_handler(modem->service, NULL, NULL);
	mmsd_disconnect_from_sms_wap();

	g_signal_handler_disconnect(gdbus_modem,
				    modem->modem_state_watch_id);
	g_object_unref(modem->sim);
	g_free(modem->imsi);
	g_free(modem->path);
	g_clear_object(&modem->modem);
	g_clear_object(&modem->modem_messaging);
	modem->object = NULL;
	if(modem->device_arr && modem->device_arr->len) {
		g_ptr_array_set_size(modem->device_arr, 0);
		g_ptr_array_unref(modem->device_arr);
	}
	modem->modem_available = FALSE;
	modem->modem_ready = FALSE;
}

static int modemmanager_init(void)
{
	GDBusConnection	*connection = mms_dbus_get_connection ();
	g_autoptr(GError) error = NULL;
	
	DBG("Starting Modem Manager Plugin!");
	// Set up modem Structure to be used here
	modem = g_try_new0(struct modem_data, 1);

	if(modem == NULL) {
		mms_error("Could not allocate space for modem data!");
		return -ENOMEM;
	}
	//TODO: Reactivate this function
//	modem->service = mms_service_create();
//	mms_service_set_identity(modem->service, IDENTIFIER);

	modem->modemsettings = mms_settings_open(IDENTIFIER, SETTINGS_STORE);

	modem->enable_vvm = g_key_file_get_boolean(modem->modemsettings, 
						   SETTINGS_GROUP,
						   "VVMEnabled", 
						   &error);
	if(error) {
		mms_error("Enabling Visual Voicemail was not configured! Setting to FALSE.");
		modem->enable_vvm = FALSE;

		g_key_file_set_boolean(modem->modemsettings, SETTINGS_GROUP,
				       "VVMEnabled", modem->enable_vvm);
		error = NULL;
	}

	modem->vvm_type = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "VVMType", &error);
	if(error) {
		modem->vvm_type = g_strdup("type_invalid");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "VVMType", modem->vvm_type);
		error = NULL;
	}

	modem->vvm_destination_number = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "VVMDestinationNumber", &error);
	if(error) {
		modem->vvm_destination_number = g_strdup("number_invalid");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "VVMDestinationNumber", modem->vvm_destination_number);
		error = NULL;
	}

	modem->carrier_prefix = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "CarrierPrefix", &error);
	if(error) {
		modem->carrier_prefix = g_strdup("//VVM");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "CarrierPrefix", modem->carrier_prefix);
		error = NULL;
	}

	modem->default_number = g_key_file_get_string(modem->modemsettings, 
						 SETTINGS_GROUP,
						 "DefaultModemNumber", &error);
	if(error) {
		mms_error("No Default Modem Number was configured! Setting to NULL.");
		modem->default_number = g_strdup("NULL");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "DefaultModemNumber", modem->default_number);
		error = NULL;
	}
	if(g_strcmp0(modem->default_number, "NULL") == 0) {
		g_free(modem->default_number);
		modem->default_number = NULL;
	}
	
	modem->provision_status = g_key_file_get_integer(modem->modemsettings, 
						        SETTINGS_GROUP,
						        "ProvisionStatus", &error);

	//If the user did not enable VVM, set provision status to "Not Set"
	if(error) {
		modem->provision_status = VVM_PROVISION_STATUS_NOT_SET;

		g_key_file_set_integer(modem->modemsettings, SETTINGS_GROUP,
				      "ProvisionStatus", modem->provision_status);
		error = NULL;
	}

	modem->mailbox_hostname = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "MailboxHostname", &error);
	if(error) {
		modem->mailbox_hostname = g_strdup("mailbox.invalid");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "MailboxHostname", modem->mailbox_hostname);
		error = NULL;
	}

	modem->mailbox_port = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "MailboxPort", &error);
	if(error) {
		// 143 is the default non-secure IMAP port
		modem->mailbox_port = g_strdup("143");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "MailboxPort", modem->mailbox_port);
		error = NULL;
	}

	modem->mailbox_username = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "MailboxUsername", &error);
	if(error) {
		modem->mailbox_username = g_strdup("username_invalid");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "MailboxUsername", modem->mailbox_username);
		error = NULL;
	}

	modem->mailbox_password = g_key_file_get_string(modem->modemsettings, 
						      SETTINGS_GROUP,
						      "MailboxPassword", &error);
	if(error) {
		modem->mailbox_password = g_strdup("password_invalid");

		g_key_file_set_string(modem->modemsettings, SETTINGS_GROUP,
				      "MailboxPassword", modem->mailbox_password);
		error = NULL;
	}

	//TODO: Fix this when you add service.c
	//mms_settings_close(IDENTIFIER, SETTINGS_STORE, modem->modemsettings, TRUE);
	//modem->modemsettings = NULL;
	introspection_data = g_dbus_node_info_new_for_xml(introspection_xml_modemmanager, NULL);
	g_assert(introspection_data != NULL);

	modem->modem_available = FALSE;
	modem->modem_ready = FALSE;
	modem->manager_available = FALSE;
	modem->context_active = FALSE;
	modem->plugin_registered = FALSE;
	modem->get_all_sms_timeout = FALSE;

	modem->registration_id = g_dbus_connection_register_object(connection,
								   MMS_PATH,
								   introspection_data->interfaces[0],
								   &interface_vtable,
								   NULL,
								   NULL,	
								   &error);

	if(error) {
		mms_error("Error registering MMSD ModemManager interface: %s\n", error->message);
		error = NULL;
	}

	modem->mm_watch_id = g_bus_watch_name(G_BUS_TYPE_SYSTEM,
					      MM_DBUS_SERVICE,
					      G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
					      (GBusNameAppearedCallback) mm_appeared_cb,
					      (GBusNameVanishedCallback) mm_vanished_cb,
					      NULL, 
					      NULL);

	return 0;
}

static void modemmanager_exit(void)
{
	GDBusConnection	*connection = mms_dbus_get_connection ();

	//TODO: Remove this when you add service.c
	mms_settings_close(IDENTIFIER, SETTINGS_STORE, modem->modemsettings, TRUE);

	if(modem->plugin_registered == TRUE) {
//		mms_service_unregister(modem->service);
		modem->modemsettings = NULL;
	}
	if(modem->modem_available) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MODEM);
	}
	if(modem->manager_available) {
		mmsd_mm_state(MMSD_MM_STATE_NO_MANAGER);
	}
	g_dbus_connection_unregister_object(connection,
					    modem->registration_id);

	g_free(modem->carrier_prefix);
	g_free(modem->default_number);
	g_free(modem->vvm_type);
	g_free(modem->mailbox_hostname);
	g_free(modem->mailbox_port);
	g_free(modem->vvm_destination_number);
	g_free(modem->mailbox_username);
	g_free(modem->mailbox_password);
	g_free(modem);
	g_dbus_node_info_unref(introspection_data);
}

MMS_PLUGIN_DEFINE(modemmanager, modemmanager_init, modemmanager_exit)

