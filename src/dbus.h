/*
 *
 *  Multimedia Messaging Service Daemon - The Next Generation
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <gio/gio.h>

#define MMS_SERVICE	"org.kop316.vvm"
#define MMS_PATH	"/org/kop316/vvm"

#define MMS_MANAGER_INTERFACE	MMS_SERVICE ".Manager"
#define MMS_SERVICE_INTERFACE	MMS_SERVICE ".Service"
#define MMS_MESSAGE_INTERFACE	MMS_SERVICE ".Message"

#define MMS_ERROR_INTERFACE	MMS_SERVICE ".Error"

GDBusConnection *mms_dbus_get_connection(void);
GDBusNodeInfo   *mms_dbus_get_introspection_data(void);
void __mms_dbus_set_connection(GDBusConnection *conn);
void __mms_dbus_set_introspection_data(void);
void __mms_dbus_unref_introspection_data(void);

/* ---------------------------------------------------------------------------------------------------- */

/*
 * GIO Dbus Server modified from here:
 * https://gitlab.gnome.org/GNOME/glib/-/blob/master/gio/tests/gdbus-example-server.c
 */

/* Introspection data for the service we are exporting */
static const gchar introspection_xml[] =
  "<node>"
  "  <interface name='org.ofono.mms.Service'>"
  "    <method name='GetMessages'>"
  "      <arg type='a(oa{sv})' name='messages_with_properties' direction='out'/>"
  "    </method>"
  "    <method name='GetProperties'>"
  "      <arg type='a{sv}' name='properties' direction='out'/>"
  "    </method>"
  "    <method name='SendMessage'>"
  "      <arg type='as' name='recipients' direction='in'/>"
  "      <arg type='v' name='options' direction='in'/>"
  "      <arg type='a(sss)' name='attachments' direction='in'/>"
  "      <arg type='o' name='path' direction='out'/>"
  "    </method>"
  "    <method name='SetProperty'>"
  "      <arg type='s' name='property' direction='in'/>"
  "      <arg type='v' name='value' direction='in'/>"
  "    </method>"
  "    <signal name='MessageAdded'>"
  "      <arg type='o' name='path'/>"
  "      <arg type='a{sv}' name='properties'/>"
  "    </signal>"
  "    <signal name='MessageRemoved'>"
  "      <arg type='o' name='path'/>"
  "    </signal>"
  "  </interface>"
  "  <interface name='org.ofono.mms.Manager'>"
  "    <method name='GetServices'>"
  "      <arg type='a(oa{sv})' name='services_with_properties' direction='out'/>"
  "    </method>"
  "    <signal name='ServiceAdded'>"
  "      <arg type='o' name='path'/>"
  "      <arg type='a{sv}' name='properties'/>"
  "    </signal>"
  "    <signal name='ServiceRemoved'>"
  "      <arg type='o' name='path'/>"
  "    </signal>"
  "  </interface>" 
  "  <interface name='org.ofono.mms.Message'>"
  "    <method name='Delete'/>"
  "    <method name='MarkRead'/>"
  "    <signal name='PropertyChanged'>"
  "      <arg type='s' name='name'/>"
  "      <arg type='v' name='value'/>"
  "    </signal>"
  "  </interface>" 
  "</node>";

/* ---------------------------------------------------------------------------------------------------- */
